#
# This makefile contains command line tools to maintain this project
# Portability at this level is accidental, only LispWorks on Mac OS X is supported here
# For some operations, edit the necessary variables to suit your environment
# Some operations can obviously only be done by a specific person in a very specific context ;-)
#

default:
	@echo Welcome to CL-SOAP, a Common Lisp implementation of SOAP
	@echo
	@echo Possible makefile targets:
	@echo
	@echo clean-fasl --- remove all known lisp compiled fasl files recursively
	@echo clean-emacs --- remove all '*~' recursively
	@echo clean --- all of the above
	@echo dist-clean --- remove all generated files and archives
	@echo compile --- compile the project through ASDF
	@echo check --- run all unit and functional tests for this project
	@echo test --- run all unit and functional tests for this project
	@echo release --- make a formal, public release
	@echo metrics --- calculate some loc metrics

clean-fasl:
	find . -name "*.fas" | xargs rm
	find . -name "*.lib" | xargs rm
	find . -name "*.nfasl" | xargs rm
	find . -name "*.dfsl" | xargs rm
	find . -name "*.fasl" | xargs rm

clean-emacs:
	find . -name "*~" | xargs rm

clean: clean-fasl clean-emacs

dist-clean: clean
	rm -rf *.tar.gz
	rm -rf *.asc

metrics:
	find src -name "*.lisp" | xargs wc -l
	find test -name "*.lisp" | xargs wc -l

LISP=/Applications/LispWorks/lispworks-tty
PRJ=cl-soap

compile:
	echo "(asdf:oos 'asdf:compile-op :$(PRJ)) :ok" | $(LISP) 

DIR=`pwd`/
SRCDIR=$(DIR)src/
TESTDIR=$(DIR)test/

test: check

check: 
	echo "(asdf:oos 'asdf:load-op :$(PRJ)) (load \"$(TESTDIR)all-tests.lisp\") :ok" | $(LISP)

ACCOUNT=scaekenberghe
CVSRT=:ext:$(ACCOUNT)@common-lisp.net:/project/$(PRJ)/cvsroot

release: test
	rm -rf /tmp/$(PRJ) /tmp/public_html /tmp/$(PRJ).tgz /tmp/$(PRJ).tgz.asc
	cd /tmp; cvs -d$(CVSRT) export -r HEAD $(PRJ); cvs -d$(CVSRT) export -r HEAD public_html
	mv /tmp/public_html /tmp/$(PRJ)/doc
	cd /tmp; gnutar cvfz $(PRJ).tgz $(PRJ); gpg -a -b $(PRJ).tgz
	scp /tmp/$(PRJ).tgz $(ACCOUNT)@common-lisp.net:/project/$(PRJ)/public_html
	scp /tmp/$(PRJ).tgz.asc $(ACCOUNT)@common-lisp.net:/project/$(PRJ)/public_html

# EOF
